﻿public class Cell
{
    public Cell(int indexCellX, int indexCellY, string textNumber, int id)
    {
        IndexCellX = indexCellX;
        IndexCellY = indexCellY;
        TextNumber = textNumber;
        IdCell = id;
    }


    public int IndexCellX { get; private set; }

    public int IndexCellY { get; private set; }

    public int IdCell { get; private set; }


    public string TextNumber { get; private set; }
}