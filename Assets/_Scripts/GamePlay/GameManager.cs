﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] private List<CellView> _listOfCells;
    [SerializeField] private List<CellView> _new = new List<CellView>();
    [SerializeField] private Transform _content;
    [SerializeField] private GameObject _cellPrefab;
    [SerializeField] private float _cellsOffset;
    [SerializeField] private float _startOffset;
    [SerializeField] private CellView _freeCell;
    [SerializeField] private Text _currentStepsUI;
    [SerializeField] private Text _winScoreText;
    [SerializeField] private GameObject _winDialog;


    public const int X_VALUE = 4;
    public const int Y_VALUE = 4;

    public int Steps { get; private set; }

    public CellView FreeCell
    {
        get { return _freeCell; }
        set { _freeCell = value; }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void Start()
    {
        GenerateField();
        UpdateUi();
    }

    public void GenerateField()
    {
        float yOffset = 0;

        float xOffset = 0;
        Steps = 0;

        List<Cell> generatedCells = new CellGenerator().GenerateRandomCells(X_VALUE * Y_VALUE);
        int currentCellNumber = 0;

        for (int y = 0; y < Y_VALUE; y++)
        {
            yOffset -= _cellsOffset;

            for (int x = 0; x < X_VALUE; x++)
            {
                if (x == 0)
                {
                    xOffset = _startOffset;
                }
                else
                {
                    xOffset += _cellsOffset;
                }

                InstantiateCell(generatedCells[currentCellNumber++], new Vector2(xOffset, yOffset));
            }
        }
    }


    public void InstantiateCell(Cell cell, Vector2 position)
    {
        var go = InstObjects(_cellPrefab, position);
        CellView cellView = go.GetComponent<CellView>();

        cellView.SetCell(position, cell.IndexCellX, cell.IndexCellY,
            cell.TextNumber, cell.IdCell);
        if (cellView.IsEmpty)
        {
            _freeCell = cellView;
        }

        _listOfCells.Add(cellView);
    }

    private GameObject InstObjects(GameObject currentPrefub, Vector2 position)
    {
        GameObject go = Instantiate(currentPrefub);
        go.transform.SetParent(_content);
        go.transform.localScale = Vector3.one;
        go.GetComponent<RectTransform>().anchoredPosition = position;
        return go;
    }

    public void UpdateUi()
    {
        _currentStepsUI.text = string.Format("STEPS : {0}", Steps);
    }

    public void CheckFinishGame()
    {
        if (!CheckRightPositions()) return;
        _winDialog.SetActive(true);
        _winScoreText.text = string.Format("Your score is : {0}", Steps);
    }

    public void Restart()
    {
        Steps = 0;
        CleanAll();
        GenerateField();
        UpdateUi();
    }

    private void CleanAll()
    {
        _listOfCells.Clear();
        foreach (Transform child in _content)
        {
            Destroy(child.gameObject);
        }
    }   

    public void ChangeBlocks(CellView cellView)
    {
        int newFreeCellX = cellView.IndexCellX;
        int newFreeCellY = cellView.IndexCellY;

        int oldFreeCellX = _freeCell.IndexCellX;
        int oldFreeCellY = _freeCell.IndexCellY;

        bool canX = oldFreeCellX - 1 == newFreeCellX || oldFreeCellX + 1 == newFreeCellX;
        bool canY = oldFreeCellY - 1 == newFreeCellY || oldFreeCellY + 1 == newFreeCellY;

        bool isChangedX = oldFreeCellX != newFreeCellX;
        bool isChangedY = oldFreeCellY != newFreeCellY;
        if (isChangedX && canX && !isChangedY || isChangedY && canY && !isChangedX)
        {
            cellView.IndexCellX = oldFreeCellX;
            cellView.IndexCellY = oldFreeCellY;

            _freeCell.IndexCellX = newFreeCellX;
            _freeCell.IndexCellY = newFreeCellY;
            
            var cellPosition = cellView.GetComponent<RectTransform>().anchoredPosition;
            var freeCelPosition = _freeCell.GetComponent<RectTransform>().anchoredPosition;

            cellView.GetComponent<RectTransform>().anchoredPosition = freeCelPosition;
            _freeCell.GetComponent<RectTransform>().anchoredPosition = cellPosition;


            int posNew = cellView.transform.GetSiblingIndex();
            int posOld = _freeCell.transform.GetSiblingIndex();

            cellView.transform.SetSiblingIndex(posOld);
            _freeCell.transform.SetSiblingIndex(posNew);
            Steps++;
            UpdateUi();
            CheckFinishGame();
        }
    }


    public bool CheckRightPositions()
    {
        var counter = 0;
       
        for (int i = 0; i < _listOfCells.Count; i++)
        {
            if (_content.transform.GetChild(i).GetComponent<CellView>().IdCellView == i + 1)
            {
                counter++;
                Debug.Log(_content.transform.GetChild(i).GetComponent<CellView>().IdCellView);
            }
        }

        if (counter == X_VALUE * Y_VALUE)
        {
            return true;
        }
        return false;
    }
}