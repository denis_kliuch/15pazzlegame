﻿using System.Collections.Generic;

public class CellGenerator
{
    public List<Cell> GenerateRandomCells(int count)
    {
        var numbersForRandom = new List<int>();
        for (int i = 1; i < count; i++)
        {
            numbersForRandom.Add(i);
        }
        MixList(numbersForRandom);

        int xMaxCount = count / GameManager.X_VALUE;
        int yMaxCount = count / GameManager.Y_VALUE;
        int cellCount = 0;
        List<Cell> cells = new List<Cell>();
        for (int i = 0; i < xMaxCount; i++)
        {
            for (int j = 0; j < yMaxCount; j++)
            {
                if (count - 1 == cellCount)
                {
                    break;
                }
                int viewNumber = numbersForRandom[cellCount++];
                cells.Add(new Cell(i, j, viewNumber.ToString(), viewNumber));
            }
        }

        cells.Add(new Cell(xMaxCount - 1, yMaxCount - 1, string.Empty, count));
        return cells;
    }


    public void MixList<T>(List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            var temp = list[i];
            var randomIndex = UnityEngine.Random.Range(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }
}