﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class CellView : MonoBehaviour, IPointerClickHandler
{
    public Vector2 CellPosition;

    public int IndexCellX; /*{ get; set; }*/

    public int IndexCellY;/*{ get; set; }*/

    public int IdCellView { get; set; }

    public bool IsEmpty { get; private set; }

    [SerializeField] private Text numberText;

    private GameManager _gameManager;


    void Start()
    {
        _gameManager = GameManager.Instance;
    }

    public void SetCell(Vector2 cellPosition, int indexCellX, int indexCellY, string numberText,
        int id)
    {
        CellPosition = cellPosition;
        IndexCellX = indexCellX;
        IndexCellY = indexCellY;
        IdCellView = id;
        this.numberText.text = numberText;
        if (string.IsNullOrEmpty(numberText))
        {
            IsEmpty = true;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!IsEmpty)
        {
            _gameManager.ChangeBlocks(this);
        }
    }
}